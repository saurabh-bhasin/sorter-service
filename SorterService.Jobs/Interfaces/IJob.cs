﻿using System.Threading;
using System.Threading.Tasks;

namespace SorterService.Jobs.Interfaces
{
    public interface IJobHandler
    {
        Task Handle(CancellationToken stoppingToken);
    }
}