﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SorterService.Jobs.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SorterService.Jobs
{
    public class BaseJob : BackgroundService
    {
        private readonly ILogger<BaseJob> _logger;
        private IServiceProvider _services { get; }

        public BaseJob(IServiceProvider services, ILogger<BaseJob> logger)
        {
            _logger = logger;
            _services = services;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                using (var scope = _services.CreateScope())
                {
                    var scopedProcessingService =
                        scope.ServiceProvider
                            .GetRequiredService<IJobHandler>();

                    await scopedProcessingService.Handle(stoppingToken);
                }

                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting Service");

            await ExecuteAsync(cancellationToken);
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping Service...");

            return Task.CompletedTask;
        }
    }
}
