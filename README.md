# sorter-service

This service helps the users to submit and retrieve jobs data and perform/update job tasks.

This service contains two main components:
1. Api 

Three main apis as per the functional requirements are:
a. GET: http://localhost:5002/v1/sorting-job/{identifier}
b. GET: http://localhost:5002/v1/sorting-job/filter/{status}
c. POST: http://localhost:5002/v1/sorting-job

2. Background Job (Timed Job running every five seconds)

Quick steps to Build & Run:
1. Clone the application in local using git clone.
2. Open the solution(SorterService.sln) in Visual Studio 2019.
3. Build the solution using Build option in Solution explorer.
4. Choose SorterService as the self hosted service in the execution option in Visual Studio.
5. Step 4 will start a command window and Swagger UI in the default web explorer.
6. Now all the apis will be excessible via Swagger UI which will act as complete documentation for APIs and the background job will be running in background.

Architecture: 
The application architecture uses in memory data wrapped under JobDataContext to query via APIs and background Job. Application is based on DI pattern to enable effective testing and maintenance in future changes.
All the sorting jobs are done in background and user is not waiting for the job to complete.
Apart from writing tests for Controller and SortingJobHandler, I have covered integration tests for Controller to see overall testing.

Improvements in future:
1. Change the background job from Timed to Pub-Sub model.
1. Implement some queue tool like RabbitMQ to leverage the full cabibility of queueing.
2. High performance sort functionality 
3. Writing api integration tests by adding docker support in the application.