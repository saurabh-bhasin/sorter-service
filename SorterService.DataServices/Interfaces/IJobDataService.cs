﻿using SorterService.DataServices.Models;
using System;
using System.Collections.Generic;

namespace SorterService.DataServices.Interfaces
{
    public interface IJobDataService
    {
        bool Exists(Guid identifier, out string message);
        JobResultModel Get(Guid identifier);
        bool ValidateStatus(string status, out string message);
        IEnumerable<JobResultModel> Get(string jobStatus);
        bool IsValidPost(int[] unsortedData, out string message);
        Guid Create(List<int> unsortedCollection);
    }
}