﻿namespace SorterService.DataServices.Interfaces
{
    public interface IDataSortingService<T> where T : struct
    {
        T[] SortData(T[] dataCollection);
    }
}