﻿using System;
using System.Collections.Generic;

namespace SorterService.DataServices.Models
{
    public class JobModel
    {
        public Guid Identifier { get; set; }
        public DateTime CreationDateTime { get; set; }
        public double Duration { get; set; }
        public JobStatus JobStatus { get; set; }
        public List<int> Input { get; set; }
        public List<int> Output { get; set; }
    }
}
