﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace SorterService.DataServices.Models
{
    public class JobDataContext
    {
        public JobDataContext()
        {
            Jobs = new List<JobModel>();
        }
        public List<JobModel> Jobs { get; set; }
    }
}
