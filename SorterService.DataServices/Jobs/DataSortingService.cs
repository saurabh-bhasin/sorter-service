﻿using SorterService.DataServices.Interfaces;
using System.Collections.Immutable;
using System.Linq;

namespace SorterService.DataServices.Jobs
{
    public class NumberSortingService : IDataSortingService<int>
    {
        /// <summary>
        /// TODO: Implement high performance sort program. Right now not focusing on it.
        /// </summary>
        /// <param name="dataCollection"></param>
        /// <returns></returns>
        public int[] SortData(int[] dataCollection)
        {
            return dataCollection.ToImmutableSortedSet().ToArray();
        }
    }
}
