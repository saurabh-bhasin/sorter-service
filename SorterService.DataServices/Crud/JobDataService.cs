﻿using SorterService.DataServices.Interfaces;
using SorterService.DataServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SorterService.DataServices.Crud
{
    public class JobDataService : IJobDataService
    {
        private readonly JobDataContext _jobDataContext;

        public JobDataService(JobDataContext jobDataContext)
        {
            _jobDataContext = jobDataContext;
        }

        public bool Exists(Guid identifier, out string message)
        {
            message = string.Empty;
            if (!_jobDataContext.Jobs.Any(x => x.Identifier == identifier))
            {
                message = $"Job {identifier} does not exist";
                return false;
            }

            return true;
        }

        public JobResultModel Get(Guid identifier) => _jobDataContext.Jobs.Where(x => x.Identifier == identifier).Select(x => new JobResultModel
        {
            Identifier = x.Identifier,
            CreationDateTime = x.CreationDateTime,
            DurationInSeconds = x.Duration,
            Input = x.Input,
            Output = x.Output,
            JobStatus = x.JobStatus.ToString()
        }).FirstOrDefault();

        public bool ValidateStatus(string status, out string message)
        {
            message = string.Empty;
            if (!Enum.IsDefined(typeof(JobStatus), status?.ToUpper()))
            {
                message = $"{status} is not valid. Only valid status are Pending/Completed/Failed";
                return false;
            }

            return true;
        }

        public IEnumerable<JobResultModel> Get(string jobStatus) => _jobDataContext.Jobs
            .Where(x => x.JobStatus == Enum.Parse<JobStatus>(jobStatus.ToUpper())).Select(x => new JobResultModel
            {
                Identifier = x.Identifier,
                CreationDateTime = x.CreationDateTime,
                DurationInSeconds = x.Duration,
                Input = x.Input,
                Output = x.Output,
                JobStatus = x.JobStatus.ToString()
            });

        public bool IsValidPost(int[] unsortedData, out string message)
        {
            message = string.Empty;
            if (unsortedData == null || !unsortedData.Any())
            {
                message = $"Input cannot be empty array";
                return false;
            }

            return true;
        }

        public Guid Create(List<int> unsortedCollection)
        {
            var newJobData = new JobModel
            {
                Identifier = Guid.NewGuid(),
                JobStatus = JobStatus.PENDING,
                Input = unsortedCollection,
                CreationDateTime = DateTime.UtcNow,
            };

            _jobDataContext.Jobs.Add(newJobData);

            return newJobData.Identifier;
        }
    }
}
