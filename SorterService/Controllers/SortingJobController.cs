﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SorterService.DataServices.Interfaces;
using SorterService.Filters.Exception;
using System;
using System.Linq;

namespace SorterService.Controllers
{
    [ApiController]
    [Route("v1.0/sorting-job")]
    [UnknownExceptionFilter]
    [BadRequestResultFilter]
    public class SortingJobController : ControllerBase
    {
        private readonly IJobDataService _jobDataService;

        private readonly ILogger<SortingJobController> _logger;

        public SortingJobController(ILogger<SortingJobController> logger, IJobDataService jobDataService)
        {
            _logger = logger;
            _jobDataService = jobDataService;
        }

        [HttpGet("{identifier:guid}")]
        public IActionResult Get(Guid identifier)
        {
            if (!_jobDataService.Exists(identifier, out string message))
            {
                return new NotFoundObjectResult(message);
            }
            return Ok(_jobDataService.Get(identifier));
        }

        [HttpGet("filter/{status}")]
        public IActionResult Get(string status)
        {
            if (!_jobDataService.ValidateStatus(status, out string message))
            {
                _logger.LogError(message);
                return new BadRequestObjectResult(message);
            }
            return Ok(_jobDataService.Get(status));
        }

        [HttpPost("numbers")]
        public IActionResult Post([FromBody] int[] unsortedData)
        {
            if (!_jobDataService.IsValidPost(unsortedData, out string message))
            {
                return new BadRequestObjectResult(message);
            }
            return Ok(_jobDataService.Create(unsortedData.ToList()));
        }
    }
}
